<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import ="java.util.Date" import = "java.time.*" import = "java.time.format.DateTimeFormatter"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSP Activity</title>
</head>
<body>

	<!-- Using the scriptlet tag, create a variable dateTime-->
	<!-- Use the LocalDateTime and DateTimeFormatter classes -->
	<!-- Change the pattern to "yyyy-MM-dd HH:mm:ss" -->
  	<!-- Using the date time variable declared above, print out the time values -->
  	<!-- Manila = currentDateTime -->
  	<!-- Japan = +1 -->
  	<!-- Germany = -7 -->
  	<!-- You can use the "plusHours" and "minusHours" method from the LocalDateTime class -->
  	<%!
	Date dateTime = new java.util.Date();
  	LocalDateTime mnl = LocalDateTime.now();
  	LocalDateTime jpn = mnl.plusHours(1);
  	LocalDateTime gmy = mnl.minusHours(7);
  	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
  	String manila = mnl.format(formatter);
  	String japan = jpn.format(formatter);
  	String germany = gmy.format(formatter);
  	
	%>
  	<h1>Our Date and Time now is...</h1>
	<ul>
		<li> Manila: <%= manila %></li>
		<li> Japan: <%= japan %></li>
		<li> Germany: <%= germany %></li>
	</ul>
	
	<!-- Given the following Java Syntax below, apply the correct JSP syntax -->
	<%!
	private int initVar=3;
	private int serviceVar=3;
	private int destroyVar=3;
	
  	public void jspInit(){
    	initVar--;
    	System.out.println("jspInit(): init"+initVar);
  		}
  	public void jspDestroy(){
    	destroyVar--;
    	destroyVar = destroyVar + initVar;
    	System.out.println("jspDestroy(): destroy"+destroyVar);
  		}{
	
  	serviceVar--;
  	System.out.println("_jspService(): service"+serviceVar);
  	String content1="content1 : "+initVar;
  	String content2="content2 : "+serviceVar;
  	String content3="content3 : "+destroyVar;
  		}
  	
  	%>
	
	<h1>JSP</h1>
	<p>content1: <%= initVar %></p>
	<p>content2: <%= serviceVar %></p>
	<p>content3: <%= destroyVar %></p>

</body>
</html>