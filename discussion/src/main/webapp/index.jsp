
<!-- 
- Imports in JSP can be achieved using directives.
- Directives are defined by the "%@" symbol which is used to defined the attribute of the page.
- Multiple imports can be achieved by adding a comma. (e.g. import= "java.util.date, java.util.scanner" 
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import ="java.util.Date"%>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
	<title>Java Server Page</title>
	</head>
	<body>
		<h1>Welcome to Hotel Servlet</h1>
		<!-- JSP allows integration of HTML tags with java syntax. can use print here -->
		<!-- JSP scriptlets -->
		<% System.out.println("Hello From  JSP"); %>
		
		<!-- JSP declaration  -->
		<%!
			Date currentDateTime = new java.util.Date();
		%>
		<!-- JSP Expression -->
		<p> The time now is <%= currentDateTime %></p>
		<!-- 
		
		JSP declaration (< %! % >)
		- allows the declaration of one or more variables or methods.
		- variables declared using the declaration tag are placed outside of __jspService Method().
		
		 -->
	<%!
		private int initVar = 0;
		private int serviceVar = 0;
		private int destroyVar = 0;
	%>
	<!-- JSP nethid Declaration -->
	
	<%!
		public void jspInit(){
			initVar++;
			System.out.println("jspInit(): init "+initVar);
		}
		public void jspDestroy(){
			destroyVar++;
			System.out.println("jspDestroy(): destroy "+destroyVar);
		}
	%>
	
	<!-- 
		JSP Scriptlets (< %%>)
			-Allow any java language statements, variable, method declarations, or expressions.
			-Variables declared using scriplet tag is placed inside __jspService() method.
	 -->
	 
	 <%
	 	serviceVar++;
	 	System.out.println("__jspService(): service" +serviceVar);
	 	
	 	String content1 = "content1: "+initVar;
	 	String content2 = "content2: "+serviceVar;
	 	String content3 = "content3: "+destroyVar;
	 %>
	 
	 <!-- 
	 	JSP Expression (< %= %>)
	 		- code placed within the JSP expression tag is written to the output stream of the response
	 		- out.println is no longer required to print values of variables/methods.
	  -->
	  
	  <h1>JSP Expression</h1>
	  <p><%= content1 %></p>
	  <p><%= content2 %></p>
	  <p><%= content3 %></p>
	  
	  <h1>Create an Account</h1>
	  <form action ="user" method="post">
	  	<label for ="firstName">First Name: </label><br>
	  	<input type="text" name="firstName"><br>
	  	
	  	<label for ="lastName">Last Name: </label><br>
	  	<input type="text" name="lastName"><br>
	  	
	  	<label for ="email">Email: </label><br>
	  	<input type="text" name="email"><br>
	  	
	  	<label for ="contact">Contact No.: </label><br>
	  	<input type="text" name="contact"><br>
	  	
	  	<input type="submit">
	  
	  </form>	</body>
</html>