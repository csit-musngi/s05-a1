package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/database")
public class DatabaseServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6668827760365993356L;
	//Mock Database
	ArrayList<String> users = new ArrayList<>();
	public void init() throws ServletException{
		System.out.println("**************************************");
		System.out.println(" DatabaseServlet has been initialized. ");
		System.out.println(" Connected to database. ");
		System.out.println("**************************************");	
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
	 ServletContext srvContext = getServletContext();
	 String firstName=srvContext.getAttribute("firstName").toString();
	 String lastName=srvContext.getAttribute("lastName").toString();
	 String email=srvContext.getAttribute("email").toString();
	 String contact=srvContext.getAttribute("contact").toString();
	 
	 users.add(firstName+" "+lastName+": "+email+" "+contact);
	 
	 PrintWriter out = res.getWriter();
	 
	 out.println(
			 "<h1>User information has been stored to the database via servlet</h1>"+
			 "<p>First Name: "+firstName+"</p>"+
			 "<p>Last Name: "+lastName+"</p>"+
			 "<p>Email: "+email+"</p>"+
			 "<p>Contact: : "+contact+"</p>"
					 
			 );
	}
	public void destroy() {
		System.out.println("**************************************");
		System.out.println(" Database has been destroyed. ");
		System.out.println(" Disconnected to database. ");
		System.out.println("**************************************");	
	}
}
